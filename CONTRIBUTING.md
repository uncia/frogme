Projet Framagit du site [GrenouilleMoi](https://grenouillemoi.fr)

---

## Comment contribuer?

Ce projet contient avant toute chose un fichier Html, ainsi que le fichier CSV qui est exposé sur le site.

Vous trouverez ce fichier dans le répertoire data, que vous pourrez télécharger, et pour lequel vous pouvez contribuer.

Si vous n’etes pas familier avec Framagit, sachez que c’est une instance Gitlab, et qu’il vous faudra avant toute chose vous créer un compte. Une branche "contributions" a été créé à cet effet.

Quelques règles tout de fois pour faciliter la vie concernant le listing CSV :

1. Si vous ajoutez une ou des lignes, merci de les rajouter à la fin
2. Merci de respecter le formalisme
3. Ce fichier a vocation de ne contenir que des solutions d’entreprise française
4. Merci de compléter tous les champs et non juste un ou deux
5. Vous pouvez toujours créer des issues à la place, mais cela est plutot réservé au dev/debug
6. Un commentaire qui justifie l’ajout, la modification, la suppression, sera grandement apprécié
7. Si vous souhaitez rajouter une colonne, merci à tout du moins de la rajouter en dernière colonne, et de rajouter un point virgule à chaque fin de ligne
